__author__ = 'Alexendar Reinaldo Perez'

#####################
#                   #
#   Introduction    #
#                   #
#####################

"""module for FASTQ file assessment"""

#################
#               #
#   Libraries   #
#               #
#################

import os
import sys
import argparse
import binascii
import pkg_resources

#############
#           #
#   Class   #
#           #
#############

class Fastq():
	def __init__(self,input_file,outdir,fast):
		self.input_file = input_file
		self.outdir = outdir
		self.fast = fast

	def file_compression_assessment(self):
		"""assess whether file is gzip compressed

		:param input_file: absolute filepath to input file
		:return: boolean

		"""
		with open(self.input_file, 'rb') as infile:
			self.value = binascii.hexlify(infile.read(2)) == b'1f8b'

	def compression_triage(self):
		"""Fastq quality assessment commands based on whether file compressed or not

		:param input_file: absolute filepath to input file
		:param outdir: absolute filepath to output directory
		:return: processed file directory and zip

		"""
		self.file_compression_assessment()
		cmd1 = '%s --outdir %s %s' % (self.fast, self.outdir, self.input_file)
		cmd2 = 'cat %s | gunzip | %s --outdir %s %s' % (
			self.input_file, self.fast, self.outdir, self.input_file)
		if self.value == True:
			sys.stdout.write('\n---\n%s\n---\n' % cmd2)
			os.system(cmd1)
		else:
			os.system(cmd2)
			sys.stdout.write('\n---\n%s\n---\n' % cmd1)
		sys.stdout.write('compression triage processing complete for \n%s\n' % self.input_file)

#########################
#                       #
#   Auxillary Function  #
#                       #
#########################

def arg_parser():
	parser = argparse.ArgumentParser()
	parser.add_argument('-i','--input',help='absolute filepath to input file',required=True)
	parser.add_argument('-o','--outdir',help='absolute filepath to output directory',required=True)

	args = parser.parse_args()
	input_file = args.input
	outdir = args.outdir

	return input_file,outdir

def file_processing(indir, outdir):
	"""extract files in input directory

	:param indir: absolute filepath to input directory
	:param outdir: absolute filepath to output directory
	:return: FASTQC processed directories and zip

	"""

	DATA_PATH = pkg_resources.resource_filename('qasar', 'fastqc/fastqc')

	for file in os.listdir(indir):
		if os.path.isfile('%s/%s' % (indir, file)):
			try:
				sys.stdout.write('\nfile: %s\n' % ('%s/%s' % (indir, file)))
				f = Fastq('%s/%s' % (indir, file), outdir, '%s' % DATA_PATH)
				f.compression_triage()
			except:
				sys.stderr.write('%s unable to be processed: Reject\n' % ('%s/%s' % (indir, file)))

#####################
#                   #
#   Main Function   #
#                   #
#####################

def main():
	"""
	infile = '/Users/Xerez/Projects/Perez/future_of_care/future_of_care/future_of_care/data/test_data/fastq/sra_data.fastq.gz'
	outdir = '/Users/Xerez/Projects/Perez/future_of_care/future_of_care/future_of_care/data/test_data/fastq'
	DATA_PATH = pkg_resources.resource_filename('qasar', 'fastqc/fastqc')
	cmd2 = 'cat %s | gunzip | %s --outdir %s %s' % (infile,DATA_PATH,outdir,infile)
	os.system(cmd2)
	sys.stdout.write('complete\n')

	with open(infile, 'rb') as in_file:
		value = binascii.hexlify(in_file.read(2)) == b'1f8b'
	"""

	# user inputs
	indir,outdir = arg_parser()

	# processing
	file_processing(indir, outdir)

	# user end message
	sys.stdout.write('\n----PROCESSING COMPLETE----\n')

if __name__ == '__main__':
	main()


