from setuptools import setup

setup(name='qasar2',
      version='0.1',
      author='xerez',
      license='MIT',
      install_requires=['htseq'],
      #dependency_links=['https://github.com/pysam-developers/pysam','https://github.com/simon-anders/htseq.git'],
      packages=['qasar'],
      include_package_data=True,
      entry_points={
	      'console_scripts':['qasar2_fastq=qasar.fast_qasar:main']},
      zip_safe=True)
